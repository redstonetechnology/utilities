<?php

namespace RedstoneTechnology\Utilities;

/**
 * Class Curl
 * @package RedstoneTechnology\Utilities
 */
class Curl
{

    /**
     * @param $url
     * @return bool|mixed
     */
    public function runRequests($url)
    {
        $curlHandler = $this->setupHandler($url);
        if (is_array($curlHandler)) {
            $curlMultiHandler = $this->setupMultiHandler($curlHandler);
            return $this->processMultipleRequests($curlMultiHandler, $curlHandler);
        }
        return $this->processRequest($curlHandler);
    }

    /**
     * @param $url
     * @return array|resource
     */
    public function setupHandler($url)
    {
        if (is_array($url)) {
            foreach ($url as $singleUrl) {
                $curlHandler[] = $this->setupHandler($singleUrl);
            }
            return $curlHandler;
        }

        $curlHandler = curl_init();
        curl_setopt_array($curlHandler, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_ENCODING => "gzip",
            CURLOPT_URL => $url,
            CURLOPT_CONNECTTIMEOUT => 6,
            CURLOPT_TIMEOUT => 12
        ));
        return $curlHandler;
    }

    /**
     * @param $curlHandlers
     * @return resource
     */
    public function setupMultiHandler($curlHandlers)
    {

        $curlMultiHandler = curl_multi_init();
        foreach ($curlHandlers as $curlHandler) {
            curl_multi_add_handle($curlMultiHandler, $curlHandler);
        }
        return $curlMultiHandler;
    }

    /**
     * @param $curlHandler
     * @return bool|mixed
     */
    public function processRequest($curlHandler)
    {
        $result = curl_exec($curlHandler);
        $curlError = curl_error($curlHandler);
        if ($curlError !== '') {
            user_error(get_class().'::processRequest - Curl Error: '.$curlError);
        }
        if ($result) {
            return $result;
        }

        return false;

    }

    /**
     * @param $curlMultiHandler
     * @param $curlHandlers
     * @return mixed
     */
    public function processMultipleRequests($curlMultiHandler, $curlHandlers)
    {
        $active = null;
        /**/
        do {
            $mrc = curl_multi_exec($curlMultiHandler, $active);
        } while ($mrc === CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc === CURLM_OK) {
           // add this line
            while (curl_multi_exec($curlMultiHandler, $active) === CURLM_CALL_MULTI_PERFORM) {
            }

            if (curl_multi_select($curlMultiHandler) != -1) {
                do {
                    $mrc = curl_multi_exec($curlMultiHandler, $active);
                } while ($mrc === CURLM_CALL_MULTI_PERFORM);
            }
        }

        foreach ($curlHandlers as $handlerId => $curlHandler) {
            $results[$handlerId] = curl_multi_getcontent($curlHandler);
            $curlError = curl_error($curlHandler);
            if ($curlError !== '') {
                user_error(get_class().'::processMultipleRequests - Curl Error: '.$curlError);
            }
            curl_multi_remove_handle($curlMultiHandler, $curlHandler);
        }
        curl_multi_close($curlMultiHandler);
        return $results;
    }
}
