<?php

namespace RedstoneTechnology\Utilities;

/**
 * Class BuildLink
 * @package RedstoneTechnology\Utilities
 */
class BuildLink
{
    protected $utilities;
    protected $contentAliases;

    /**
     * BuildLink constructor.
     * @param Utilities $utilities
     * @param array $contentAliases
     */
    public function __construct(
        \RedstoneTechnology\Utilities\Utilities $utilities,
        array $contentAliases = ['content', 'articles']
    ) {
        $this->utilities = $utilities;
        $this->contentAliases = $contentAliases;
    }

    /**
     * @param bool|false $urlString
     * @return array
     */
    public function urlToArray($urlString = false)
    {
        if (empty($urlString)) {
            $urlString = $this->utilities->server('REQUEST_URI');
        }
        $urlString = strtok($urlString, '?');
        if ($urlString !== '/') {
            $url = explode("/", trim($urlString, "/"));
            $request = $this->processUrlArray($url);
        }
        if (isset($request)) {
            return $request;
        }
        return ['tool' => 'local', 'action' => 'index'];
    }

    /**
     * @param $url
     * @return mixed
     */
    protected function processUrlArray($url)
    {
        if (!empty($url[0]) && $url[0] === 'api') {
            $request['api'] = 'true';
            array_shift($url);
        } else {
            $request['api'] = 'false';
        }
        if (!empty($url[0])) {
            $request['tool'] = in_array($url[0], $this->contentAliases) ? 'content' : $url[0];
            unset($url[0]);
        }
        if (!empty($url[1])) {
            if ($request['tool'] !== 'content') {
                $request['action'] = $url[1];
                unset($url[1]);
            } else {
                $request['tool'] = 'content';
                $request['contentid'] = $url[1];
                $request['action'] = 'index';
            }
        } else {
            $request['action'] = 'index';
            if ($request['tool'] === 'content') {
                $request['contentid'] = 'index';
            }
        }
        $segmentParity = 1;
        $key = '';
        foreach ($url as $url_segment) {
            if ($segmentParity === 1) {
                $key = $url_segment;
                $segmentParity = 0;
            } else {
                $request[$key] = $url_segment;
                $segmentParity = 1;
            }
        }
        return $request;
    }
}
