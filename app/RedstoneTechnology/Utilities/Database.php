<?php

/**
 * A simple class to manage database connections to MySQL using PDO
 * (allowing us to change database engine later without much disruption).
 *
 * <code>
 * $database = new Database();
 *
 * print_r($database->readQuery('select * from requests'));
 * </code>
 *
 * @author Tom
 */
namespace RedstoneTechnology\Utilities;

/**
 * Class Database
 * @package RedstoneTechnology\Utilities
 */
class Database extends \PDO
{
    public $connected;
    protected $table;
    protected $statement;
    protected $databaseConfig;
    protected $requiredParams = [
        'server',
        'port',
        'database',
        'username',
        'password',
    ];

    /**
     * Construct the Database object, using a $this->databaseConfig array, provides
     * helper functions, while still providing a usable PDO object.
     *
     * @param Configuration $configuration
     * @param null $databaseConfig
     * @internal param array $this->databaseConfig Supplies configuration options to the object
     */
    public function __construct(
        $databaseConfig
    ) {
        $this->databaseConfig = $databaseConfig;
        if($this->testConfig($this->databaseConfig) === false) {
            throw new \Exception("Config is not valid.");
        }
        try {
            parent::__construct(
                'mysql:host='.$this->databaseConfig['server'].
                ';port='.$this->databaseConfig['port'].
                ';dbname='.$this->databaseConfig['database'],
                $this->databaseConfig['username'],
                $this->databaseConfig['password'],
                array(
                    \PDO::MYSQL_ATTR_LOCAL_INFILE => true
                )
            );
            $this->connected = true;
            if (isset($this->databaseConfig['table'])) {
                $this->table = $this->databaseConfig['table'];
            }
            return true;
        } catch (\PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
            $this->connected = false;
            return false;
        } catch (\Exception $e) {
            echo 'Connection failed, wasn\'t a PDOException : ' . $e->getMessage();
            $this->connected = false;
            return false;
        }
    }

    public function testConfig($config) {
        $errors = [];
        foreach($this->requiredParams as $requiredParam) {
            if(empty($config[$requiredParam])) {
                $errors = "Required param {$requiredParam} is not set.\n";
            }
        }
        if(!empty($errors)) {
            throw new \Exception("Required params missing:\n".explode("\n", $errors));
        }
    }

    /**
     * @return mixed
     */
    public function getDatabaseConfig()
    {
        return $this->databaseConfig;
    }

    /**
     *
     * @param string $table should be the name of the table we're working with,
     *                      is used to check column names or count rows. Passing nothing will clear
     *                      the value for use next time.
     */
    public function setTable($table = null)
    {
        if (isset($table)) {
            $this->table = $table;
        } elseif (isset($this->table)) {
            unset($this->table);
        }
    }

    /**
     * @return bool
     * @internal param string $table should be the name of the table we're working with,
     * is used to check column names or count rows.
     */
    public function getTable()
    {
        if (isset($this->table)) {
            return $this->table;
        }

        return false;
    }

    /**
     *
     * @param  string $userQuery contains the SQL to be written. Currently does
     * nothing more than the runQuery() function, but may in future.
     * @param array $data
     * @return PDO ::StatementHandler
     */
    public function writeQuery($userQuery, $data = [])
    {
        return $this->runQuery($userQuery, $data);
    }

    /**
     * @param $userQuery
     * @param $data
     * @throws \Exception
     */
    public function manyWriteQuery($userQuery, $data)
    {
        $this->statement = $this->prepare($userQuery);
        if (empty($data)) {
            throw new \Exception('No data provided');
        }
        echo "$userQuery\n";
        foreach ($data as $dataRow) {
            if (!is_array($dataRow)) {
                throw new \Exception("Data is not a multi-dimensional array.");
            }
            $this->statement->execute($dataRow);
        }

    }

    /**
     *
     * @param  string $userQuery contains the SQL to be read. Currently does
     * nothing more than the runQuery() function, but may in future.
     * @param array $data
     * @return PDO ::StatementHandler
     */
    public function readQuery($userQuery, $data = [])
    {
        return $this->runQuery($userQuery, $data);
    }

    /**
     *
     * @param  string $userQuery contains the SQL to be run. Checks the
     * connection is active, and runs the SQL
     * @param array $data
     * @return PDO ::StatementHandler
     */
    public function runQuery($userQuery, $data = [])
    {
        if ($this->connected === true) {
            try {
                if (!empty($data)) {
                    $this->statement = $this->prepare($userQuery);
                    $this->statement->execute($data);
                } else {
                    $this->statement = $this->query($userQuery);
                }
                if (!$this->statement) {
                    $errorInfo = $this->errorInfo();
                    throw new \Exception("Error occured running query: ".$errorInfo[2]);
                } else {
                    $this->statement->setFetchMode(\PDO::FETCH_ASSOC);

                    return $this->statement;
                }
            } catch (\PDOException $e) {
                $this->logger->error("Database::runQuery: PDOException: ".$e->getMessage());
            }
        } else {
            throw new \Exception("Database::runQuery - Query failed, DB Connection is ".
                ($this->connected === true ? 'true' : 'false')
            );
        }
    }

    /**
     * The function should return the number of rows a query returns.
     * @param  string $sql this can either be the count query,
     *                       or the query that needs modifying to give a count.
     * @param  bool $regex if this is true, the query is modified to be a count.
     * @param array $data
     * @return int this is the number of rows counted.
     */
    public function rowCount($sql = '', $regex = true, $data = [])
    {
        if (isset($this->statement) && $this->statement->rowCount() > 0 && $this->connected === true) {
            try {
                $rows = $this->statement->rowCount();
            } catch (\Exception $e) {
                return false;
            }

            return $rows;
        }
        return false;
    }

    /**
     * The function returns the columns names, by looking it up
     * in the information schema.
     *
     * @param bool $table
     * @return array this is an array of the columns of the table.
     */
    public function getColumnNames($table = false)
    {
        if ($table === false && isset($this->table)) {
            $table = $this->table;
        } elseif ($table === false) {
            throw new \Exception("Table name not set");
        }
        /**
         * @TODO: Needs to use database name too.
         */
        $sql = 'select column_name from information_schema.columns where '.
                'lower(table_name)=lower(\''.$table.'\')';
        $statement = $this->prepare($sql);

        try {
            if ($statement->execute()) {
                $rawColumnData = $statement->fetchAll(\PDO::FETCH_ASSOC);

                foreach ($rawColumnData as $array) {
                    foreach ($array as $inner_key => $value) {
                        if (!(int) $inner_key) {
                            $column_names[] = $value;
                        }
                    }
                }
            }
            return $column_names;
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $query
     * @return bool|string
     */
    public function loadData($query)
    {
        $database = mysqli_init();
        $database->options(MYSQLI_OPT_LOCAL_INFILE, true);
        $database->real_connect(
            $this->databaseConfig['user_server'],
            $this->databaseConfig['user_user'],
            $this->databaseConfig['user_password'],
            $this->databaseConfig['user_database']
        );

        if (!$database->query($query)) {
            return $database->error;
        }

        return true;
    }
}
