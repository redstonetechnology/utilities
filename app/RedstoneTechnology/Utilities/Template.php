<?php
/**
 * This class aims to provide a link between our use of templates, and a template library, making migration simpler.
 * User: thomasredstone
 * Date: 01/07/15
 * Time: 22:44
 */
namespace RedstoneTechnology\Utilities;

/**
 * Class Template
 * @package RedstoneTechnology\Utilities
 */
class Template
{
    protected $template;

    /**
     * @param $template
     */
    public function __construct(
        $template
    ) {
        $this->template = $template;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function share($data)
    {
        return $this->template->share($data);
    }

    /**
     * @param $view
     * @param array $data
     * @return mixed
     */
    public function make($view, $data = [])
    {
        return $this->template->make($view, $data);
    }

    /**
     * @param $view
     * @return mixed
     */
    public function exists($view)
    {
        return $this->template->exists($view);
    }
}
