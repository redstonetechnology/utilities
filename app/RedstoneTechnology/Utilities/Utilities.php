<?php
namespace RedstoneTechnology\Utilities;

/**
 * Class Utilities
 * @package RedstoneTechnology\Utilities
 */
class Utilities
{
    protected $serverArray;
    protected $globals;
    protected $mobileDetect;

    /**
     * @param $mobileDetect
     * @param $globals
     */
    public function __construct($mobileDetect, $globals)
    {
        $this->globals = $globals;
        $this->serverArray = [];
        $this->mobileDetect = $mobileDetect;
    }

    /**
     * @param $name
     * @param $value
     * @param $expires
     * @return bool
     */
    public function writeCookie($name, $value, $expires)
    {
        $domain = Configuration::$cookieDomain;
        if (php_sapi_name() !== 'cli' && $name != 'postcode') {
            if (setcookie($name, $value, $expires, '/', $domain)) {
                return true;
            }
        }
        return false;
    }

    /**
     * This function is designed to catch instances where a required variable
     * is missing somewhere, and will cause a fatal error by default.
     *
     * @param array $variables contains all of the variables that should be set
     *                         before the calling function can proceed.
     */
    public function checkRequired($variables)
    {
        $fail = false;
        foreach ($variables as $id => $variable) {
            if ($variable === false) {
                echo "<h1>The variable \"$id\" is not set.</h1>";
                $error = (isset($error) ? "$error, $id" : $id);
                $fail = true;
            }
            if ($fail === true) {
                trigger_error(
                    "The variable(s): $error are required, but not set.",
                    E_USER_ERROR
                );
            }
        }
    }

    public function getRealIpAddress()
    {
        if (empty($this->serverArray)) {
            $this->serverArray = $this->server();
        }
        if (!empty($this->serverArray['HTTP_CLIENT_IP'])) {
            //check ip from share internet
            $ipAddress = $this->serverArray['HTTP_CLIENT_IP'];
        } elseif (!empty($this->serverArray['HTTP_X_FORWARDED_FOR'])) {
            //to check ip is pass from proxy
            $ipAddress = $this->serverArray['HTTP_X_FORWARDED_FOR'];
        } else {
            $ipAddress = $this->serverArray['REMOTE_ADDR'];
        }

        return $ipAddress;
    }

    /**
     * @param $str
     * @return bool
     */
    public function isNotInjected($str)
    {
        $injections = array('(\n+)',
                            '(\r+)',
                            '(\t+)',
                            '(%0A+)',
                            '(%0D+)',
                            '(%08+)',
                            '(%09+)');
        $inject = join('|', $injections);
        $inject = "/$inject/i";
        if (preg_match($inject, $str)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param $regex
     * @param $array
     * @return array|bool
     */
    public function inArrayMatch($regex, $array)
    {
        foreach ($array as $key => $item) {
            if (preg_match($regex, $key, $match)) {
                $matches[] = $match[0];
            }
        }
        if (isset($matches)) {
            return $matches;
        } else {
            return false;
        }
    }

    /**
     * getChild is a function for getting elements of a SimpleXML object
     * when we know their path, but do not want to use 'eval'.
     *
     * @param  type $paramPath the full path of the variable
     * @param \SimpleXMLElement $variable
     * @return mixed if there is a problem, false is returned, otherwise the
     *                         correct child value is returned.
     * @internal param type $result the simpleXML object
     */
    public function getChild($paramPath, \SimpleXMLElement $variable)
    {
        $paramPathArray = explode('->', $paramPath);
        foreach($paramPathArray as $paramPathArrayPart) {
            if(!isset($childVariable)) {
                $childVariable = $variable->$paramPathArrayPart;
            } else {
                $childVariable = $childVariable->$paramPathArrayPart;
            }
        }
        if(!isset($childVariable)) {
            return false;
        }
        return $childVariable;
    }
}
