<?php

namespace RedstoneTechnology\Utilities;

/**
 * Class Archive
 * @package RedstoneTechnology\Utilities
 */
class Archive
{
    protected $file;
    protected $path;
    protected $name;
    protected $extension;
    protected $validExtensions = ['tar.gz', 'zip'];
    protected $basePath;
    protected $files = [];

    protected $archive;

    /**
     * @param File $file
     */
    public function __construct(
        \RedstoneTechnology\Utilities\File $file
    ) {
        $this->file = $file;
    }

    /**
     * @param $source
     * @param $destination
     * @param $extension
     * @throws \Exception
     */
    public function extractArchive($source, $destination, $extension)
    {
        if (in_array($this->extension, $this->validExtensions)) {
            throw new \Exception("Archive::__construct: Extension '{$extension}' not valid");
        }
        $this->extension = $extension;
        if ($this->extension === 'tar.gz') {
            $this->archive = new \PharData($source);
        } elseif ($this->extension === 'zip') {
            $this->archive = new \ZipArchive();
            if ($this->archive->open($source, \ZipArchive::CREATE) !== true) {
                throw new \Exception(
                    "Archive::buildArchive: cannot open <{$source}>"
                );
            }
            $this->archive->extractTo($destination);
        }

    }

    /**
     * @param $path
     * @param $name
     * @param $basePath
     * @param array $files
     * @param $extension
     * @return bool
     * @throws \Exception
     */
    public function buildArchive($path, $name, $basePath, Array $files, $extension)
    {
        if (in_array($this->extension, $this->validExtensions)) {
            throw new \Exception("Archive::__construct: Extension '{$extension}' not valid");
        }
        $this->extension = $extension;

        $this->path = $path;
        $this->name = $name;
        $this->basePath = $basePath;
        $this->files = $files;
        chdir($this->basePath);

        $this->prepareFile();

        if ($this->extension === 'tar.gz') {
            $this->archive = new \PharData("{$this->path}/{$this->name}.tar");
        } elseif ($this->extension === 'zip') {
            $this->archive = new \ZipArchive();
            if ($this->archive->open("{$this->path}{$this->name}.{$this->extension}", \ZipArchive::CREATE) !== true) {
                throw new \Exception(
                    "Archive::buildArchive: cannot open <{$this->path}/{$this->name}.{$this->extension}>"
                );
            }
        }
        echo "Extension: {$this->extension}\n";

        $this->files = $this->file->expandFileList($this->basePath, $files);

        $this->addResources();

        echo "Creating Archive at \"{$this->path}{$this->name}.{$this->extension}\"\n";

        $this->archive->close();
        return true;
    }

    protected function prepareFile()
    {
        if (is_file("{$this->path}{$this->name}.{$this->extension}")) {
            unlink("{$this->path}{$this->name}.{$this->extension}");
        }
        if ($this->extension === 'tar.gz' && is_file("{$this->path}{$this->name}.tar")) {
            unlink("{$this->path}{$this->name}.tar");
        }
        if (is_dir("{$this->path}{$this->name}")) {
            $this->file->deleteFiles("{$this->path}{$this->name}");
        }
    }

    /**
     * @param $files
     * @return bool
     */
    public function addFiles($files)
    {
        if (is_array($files) && !empty($files)) {
            $this->files = array_merge($this->files, $files);
            return true;
        }
        if (is_string($files) && !empty($files)) {
            $this->files[] = $files;
            return true;
        }
        return false;
    }


    protected function addResources()
    {
        $filesAdded = 0;
        foreach ($this->files as $file) {
            if (file_exists($file)) {
                if (!is_dir($file)) {
                    $this->archive->addFile($file);
                    $filesAdded++;
                }
            } else {
                echo "File \"{$file}\" doesn't exist in \"". getcwd(). "\"\n";
            }
        }
        echo "Added {$filesAdded} to archive\n";
    }
}
