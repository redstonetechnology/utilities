<?php
/**
 * A simple class to manage database connections to Redis
 * <code>
 * $cache = new Cache();
 *
 * print_r($cache->get('foo'));
 * </code>
 *
 * @author Tom
 */
namespace RedstoneTechnology\Utilities;

/**
 * Class Cache
 * A simple wrapper class, primarily for Redis, but designed to allow any other cache to be swapped out at a later date
 * without touching any of the claling classes.
 * @package RedstoneTechnology\Utilities
 */
class Cache
{
    /**
     * @var \Predis\Client
     */
    protected $cache;

    /**
     * @var bool
     */
    public $connected;

    /**
     * @param \Predis\Client $redis
     */
    public function __construct(
        \Predis\Client $redis
    ) {
        $this->cache = $redis;
        $this->connected = true;
    }

    /**
     * @param $key
     * @param $field
     * @param $value
     * @param bool|false $expiry
     * @return mixed
     */
    public function set($key, $field, $value, $expiry = false)
    {
        $setReturn = $this->cache->hset($key, $field, $value);
        if ($expiry !== false) {
            $this->cache->expire($key, $expiry);
        }
        return $setReturn;
    }

    /**
     * @param $key
     * @param $field
     * @return string
     */
    public function get($key, $field)
    {
        if ($this->cache->hexists($key, $field) !== true) {
            return null;
        }
        return $this->cache->hget($key, $field);
    }

    /**
     * @param $key
     * @param $value
     * @param bool|false $expiry
     * @return mixed
     */
    public function singleSet($key, $value, $expiry = false)
    {
        $setReturn = $this->cache->set($key, $value);
        if ($expiry !== false) {
            $this->cache->expire($key, $expiry);
        }
        return $setReturn;
    }

    /**
     * @param $key
     * @return string
     */
    public function singleGet($key)
    {
        if ($this->cache->exists($key) !== true) {
            return null;
        }
        return $this->cache->get($key);
    }

    /**
     * @param $key
     * @param $field
     * @return int
     */
    public function remove($key, $field)
    {
        return $this->cache->hdel($key, $field);
    }

    /**
     * @param $key
     * @return array
     */
    public function getAll($key)
    {
        return $this->cache->hgetall($key);
    }

    /**
     * @param $key
     * @return int
     */
    public function ttl($key)
    {
        return $this->cache->ttl($key);
    }
}
