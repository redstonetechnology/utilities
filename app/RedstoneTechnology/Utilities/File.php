<?php

namespace RedstoneTechnology\Utilities;

/**
 * Class File
 * @package RedstoneTechnology\Utilities
 */
class File
{
    protected $logger;
    protected $adapter;

    /**
     * @param \League\Flysystem\Adapter\AbstractAdapter $adapter can use S3 or Local for now.
     * @param \League\Flysystem\Filesystem $filesystem
     * @param \League\Flysystem\Config $config
     */
    public function __construct(
        \League\Flysystem\Adapter\AbstractAdapter $adapter,
        \League\Flysystem\Filesystem $filesystem,
        \League\Flysystem\Config $config
    ) {
        $this->filesystem = $filesystem;
        $this->adapter = $adapter;
        $this->config = $config;
    }

    /**
     * For a non existant file, it will be created, otherwise it will simply return (traditionally it should update
     * the modified time, but that wasn't required for our implementation).
     * @param $file
     * @return bool
     */
    public function touch($file)
    {
        try {
            if ($this->adapter->has($file)) {
                return true;
            }
            return (bool) $this->adapter->write($file, '', $this->config);
        } catch (\Exception $e) {
            throw new \Exception('Failed to touch '.$file.' : '.$e->getMessage());
        }
    }

    /**
     * Forwards to Flysystem, and will create and write a file, or exception if it already exists.
     * @param $file
     * @param $content
     * @return array|false
     */
    public function write($file, $content)
    {
        return $this->adapter->write($file, $content, $this->config);
    }

    /**
     * Will overwrite an existing file, or exception if it doesn't already exist.
     * @param $file
     * @param $content
     * @return array|false
     */
    public function update($file, $content)
    {
        return $this->adapter->update($file, $content, $this->config);
    }

    /**
     * Will create a file, or overwrite it, as appropriate!
     * @param $file
     * @param $content
     * @return mixed
     */
    public function put($file, $content)
    {
        $result = $this->filesystem->put($file, $content);
        return $result;
    }

    /**
     * Gets the files most recently modified time.
     * @param $file
     * @return bool|int
     */
    public function modTime($file)
    {
        if (!$this->adapter->has($file)) {
            return false;
        }
        try {
            $modTime = filemtime($file);
            return $modTime;
        } catch (\Exception $e) {
            throw new \Exception('Failed to get modified time for '.$file.' : '.$e->getMessage());
        }
    }

    /**
     * Tells us if a given file exists.
     * @param $file
     * @return array|bool|null
     */
    public function exists($file)
    {
        return $this->adapter->has($file);
    }

    /**
     * @param $file
     * @return int
     */
    public function lastLineAge($file)
    {
        $lastline = $this->tail(
            $file,
            1
        );
        $timestamp = explode(',', $lastline);
        $timestamp = $timestamp[0];
        $age = time() - $timestamp;
        return $age;
    }

    /**
     * @param $dir
     * @param $age
     * @return bool
     */
    public function clearOld($dir, $age)
    {
        if (!$this->exists($dir)) {
            throw new \Exception('Directory to be cleared does not exist: '.$dir);
        }
        try {
            $lock = fopen($dir.'.lock', "w");
            if (flock($lock, LOCK_EX)) {
                if ($handle = opendir($dir)) {
                    while (false !== ($file = readdir($handle))) {
                        if ($file != "." && $file != "..") {
                            $stat = stat($dir.'/'.$file);
                            if ($stat[9] < time() - $age) {
                                unlink($dir.'/'.$file);
                            }
                        }
                    }
                }
                closedir($handle);
                fclose($lock);
                return true;
            }
            return false;
        } catch (\Exception $e) {
            throw new \Exception('Failed to clearOld in '.$dir.' : '.$e->getMessage());
        }
    }

    /**
     * This function aims to give the lines at the end of a file, the number of
     * lines is specified when it is called.
     *
     * @see http://tekkie.flashbit.net/php/tail-functionality-in-php is the code
     * this function is based on.
     *
     * @param  string $file  should be the path to a file.
     * @param  int    $lines the number of lines you wish to be returned.
     * @return type
     */
    public function tail($file, $lines)
    {
        $handle = fopen($file, "r");
        $linecounter = $lines;
        $pos = -2;
        $beginning = false;
        $text = array();
        while ($linecounter > 0) {
            $tail = " ";
            while ($tail != "\n") {
                if (fseek($handle, $pos, SEEK_END) === -1) {
                    $beginning = true;
                    break;
                }
                $tail = fgetc($handle);
                $pos --;
            }
            $linecounter --;
            if ($beginning) {
                rewind($handle);
            }
            $text[$lines-$linecounter-1] = fgets($handle);
            if ($beginning) {
                break;
            }
        }
        fclose($handle);

        return array_reverse($text);
    }

    /*
     * php delete function that deals with directories recursively
     */
    /**
     * @param $target
     * @return bool
     */
    public function deleteFiles($target)
    {

        if ($this->adapter->has($target)) {
            $rootDir = $this->adapter->getPathPrefix();
            $dirHandle = opendir("{$rootDir}/{$target}");
            if (!$dirHandle) {
                return false;
            }
            while ($file = readdir($dirHandle)) {
                if ($file != '.' && $file != '..') {
                    if (!is_dir("{$rootDir}/{$target}/{$file}")) {
                        unlink("{$rootDir}/{$target}/{$file}");
                    } else {
                        $this->deleteFiles("{$target}/{$file}");
                    }
                }
            }
            closedir($dirHandle);
            rmdir("{$rootDir}/{$target}");
            return true;
        }
        return false;
    }

    /**
     * @param $basePath
     * @param $originalFiles
     * @return array
     */
    public function expandFileList($basePath, $originalFiles)
    {
        echo "Expanding file list\n";
        foreach ($originalFiles as $file) {
            $file = trim($file, '/').'/';
            if (!file_exists($file) && file_exists("{$basePath}{$file}")) {
                $file = "{$basePath}{$file}";
            }
            echo "{$file}\n\n\n\n";
            $files[] = $file;
            if (is_dir($file)) {
                $files = array_merge($files, $this->getFileList($basePath, $file));
                echo "{$file} is a directory\n";
            } else {
                echo "{$file} is not a directory\n";
            }
        }
        return $files;
    }

    /**
     * @param $basePath
     * @param $directory
     * @return array
     */
    public function getFileList($basePath, $directory)
    {
        $directory = rtrim($directory, '/').'/';
        if (!file_exists($directory) && file_exists("{$basePath}/{$directory}")) {
            $directory = rtrim($basePath, '/').'/'.ltrim($directory, '/');
        }
        $files = [];
        $directoryList = scandir($directory);
        foreach ($directoryList as $file) {
            if (!in_array($file, ['.', '..'])) {
                if (is_dir("{$directory}/{$file}")) {
                    $subdir = $this->getFileList($basePath, rtrim($directory, '/').'/'.ltrim($file, '/'));
                    $files = array_merge($subdir, $files);
                }
                $files[] = "{$directory}{$file}";
            }
        }
        return $files;
    }

    /**
     * Creates a directory
     *
     * @param $target
     * @param int $permissions
     * @return bool
     */
    public function mkdir($target, $permissions = 0777)
    {
        return mkdir($target, $permissions);
    }

    /**
     * Returns the list of files in the directory, ordered by their age.
     * @param $dir
     * @return array
     */
    public function oldest($dir)
    {
        $files = scandir($dir);
        foreach ($files as $id => $file) {
            $files[$id] = "{$dir}{$file}";
            if (in_array($file, ['.', '..'])) {
                unset($files[$id]);
            }
        }
        return $files;
    }

    /**
     *
     * @param $rootDir
     * @return \League\Flysystem\Adapter\AbstractAdapter
     */
    public function setRoot($rootDir)
    {
        echo "\nsetting root\n";
        $this->logger->warning("Setting root to \"{$rootDir}\", was ".$this->adapter->getPathPrefix());
        echo "Setting root to \"{$rootDir}\", was ".$this->adapter->getPathPrefix()."\n";
        return $this->adapter->setPathPrefix($rootDir);
    }
}
