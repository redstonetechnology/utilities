<?php

namespace RedstoneTechnology\Utilities;

/**
 * The language class looks up a named string in a language file, which is a
 * PHP file which contains one large array of strings in a single
 * language. Returning the requested string.
 */
class Language
{
    protected $translator;
    protected $language;
    protected $defaultLanguage;
    protected $parent;
    protected $utilities;
    protected $load;
    protected $logger;
    protected $fullLanguageCodes = [
        'en' => 'en-gb',
        'au' => 'en-au',
        'de' => 'de',
        'us' => 'en-us',
        'en_GB' => 'en-gb',
        'en_AU' => 'en-au',
        'de_DE' => 'de',
        'en_US' => 'en-us',
    ];

    protected $isoLanguageCodes = [
        'en-gb' => 'en_GB',
        'en-au' => 'en_AU',
        'de' => 'de_DE',
        'en-us' => 'en_US',
        'en' => 'en_GB',
        'au' => 'en_AU',
        'us' => 'en_US',
    ];

    protected $validLanguageCodes = [
        'en_GB', 'en_AU', 'de_DE', 'en_US'
    ];

    /**
     * @param \Symfony\Component\Translation\Translator $translator
     * @param Utilities $utilities
     * @param \RedstoneTechnology\Support\Language\Load $load
     * @param \Monolog\Logger $logger
     */
    public function __construct(
        \Symfony\Component\Translation\Translator $translator,
        \RedstoneTechnology\Utilities\Utilities $utilities,
        \RedstoneTechnology\Support\Language\Load $load,
        \Monolog\Logger $logger
    ) {
        $this->utilities = $utilities;
        $this->load = $load;
        $this->translator = $this->load->addLoaders($translator, $this->validLanguageCodes);
        $this->logger = $logger;
        $this->translator->setFallbackLocales(array('en_GB'));
        $this->language = $this->translator->getLocale();

    }

    /**
     * @param $language
     */
    protected function changeLanguage($language)
    {
        $this->translator->setLocale($language);
        $this->language = $this->translator->getLocale();
    }

    /**
     * which will retrieve a named string from the language file specified,
     * if this fails, it will fall back to checking the default language for
     * the named string and return that (while logging the failure, as it is
     * not intended that there ever be differences in the language files).
     * @param  string $name
     * @param  boolean $dontComplain
     * @param  mixed $substitutions
     * @return boolean
     */
    public function getString($name, $dontComplain = false, $substitutions = false)
    {
        if (!empty($substitutions) && is_array($substitutions)) {
            $translatedString = $this->translator->trans($name, $substitutions);
        } else {
            $translatedString = $this->translator->trans($name);
        }
        if ($translatedString === $name) {
            if ($dontComplain === false) {
                $this->logger->error(
                    "Langauge::getString warning: " .
                    "String - {$name} was not found in parent language ({$this->parent})."
                );
            }
            return false;
        }
        return $translatedString;
    }

    /**
     * which will retrieve a page's content in the language specified,
     * if this fails, it will fall back to checking the fallback, and then the default
     *  language for the page and return that.
     * @param $page
     * @param $contentSegment
     * @param bool $dontComplain
     * @param bool $language
     * @return bool
     * @internal param type $name
     */
    public function getPageContent(
        $page,
        $contentSegment,
        $dontComplain = false,
        $language = false
    ) {
        if ($language === false) {
            $language = $this->language;
        }
        if ($contentSegment) {
            $path = $this->makePath($this->fullLanguageCodes[$language], $contentSegment);
            if (is_file($path)) {
                return file_get_contents($path);
            } elseif (!empty($this->parent) &&
                $language !== $this->parent &&
                $pageContent = $this->getPageContent($page, $contentSegment, $dontComplain, $this->parent)
            ) {
                return $pageContent;
            } elseif (!empty($this->defaultLanguage) &&
                $language !== $this->defaultLanguage &&
                $pageContent = $this->getPageContent($page, $contentSegment, $dontComplain, $this->defaultLanguage)
            ) {
                return $pageContent;
            } elseif ($this->language === $language && is_file($this->makePath('en', $contentSegment))) {
                return file_get_contents($this->makePath('en', $contentSegment));
            }
        }

        return false;
    }

    /**
     * @param $language
     * @param $contentSegment
     * @return string
     */
    public function makePath($language, $contentSegment)
    {
        $path = APPLICATION_PATH . "assets/resources/static/{$language}/{$contentSegment}.html";
        return $path;
    }

    /**
     * will take a regular expression, and find strings within the language
     * file which match it, using inArrayMatch, which is part of Utilities.
     * @param  string $regex
     * @return array
     */
    public function getList($regex)
    {
        $return = [];
        if ($returnPart = $this->utilities->inArrayMatch(
            $regex,
            $this->translator->getCatalogue($this->language)->all()['messages']
        )) {
            $return = $return + $returnPart;
        }
        if (!empty($this->parent) &&
            $returnPart = $this->utilities->inArrayMatch(
                $regex,
                $this->translator->getCatalogue($this->parent)->all()['messages']
            )
        ) {
            $return = $return + $returnPart;
        }
        sort($return);
        return (!empty($return) ? array_unique($return) : false);
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param $language
     * @return bool
     */
    public function setLanguage($language)
    {
        if (!in_array($language, $this->validLanguageCodes)) {
            if (!empty($this->isoLanguageCodes[$language])) {
                $language = $this->isoLanguageCodes[$language];
            }
        }
        if ($this->language = $language) {
            $this->changeLanguage($language);
            return true;
        }
        return false;
    }
}
