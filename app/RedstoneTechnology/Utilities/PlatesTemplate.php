<?php
/**
 * Created by PhpStorm.
 * User: thomasredstone
 * Date: 01/07/15
 * Time: 23:15
 */
namespace RedstoneTechnology\Utilities;

/**
 * Class PlatesTemplate
 * @package RedstoneTechnology\Utilities
 */
class PlatesTemplate extends Template
{

    /**
     * @param $key
     * @param bool|false $data
     * @return bool
     */
    public function share($key, $data = false)
    {
        if (is_array($key) && $data === false) {
            foreach ($key as $subKey => $subData) {
                $this->share($subKey, $subData);
            }
            return true;
        }
        return $this->template->addData([$key => $data]);
    }

    /**
     * @param $view
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function make($view, $data = [])
    {
        if (empty($view)) {
            throw new \Exception("Empty view requested");
        }
        if ($view === 'theme.legacy.default') {
            echo debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1]['function'];
        }
        return $this->template->render($this->reformatViewName($view), $data);
    }

    /**
     * @return mixed
     */
    public function render()
    {
        return $this->template->render();
    }

    /**
     * @param $view
     * @return mixed
     */
    public function exists($view)
    {
        return $this->template->exists($this->reformatViewName($view));
    }

    /**
     * @param $view
     * @return mixed
     */
    protected function reformatViewName($view)
    {
        $newView = str_replace('.', '/', $view);
        return $newView;
    }
}
