<?php
/**
 * Created by PhpStorm.
 * User: thomasredstone
 * Date: 15/12/2015
 * Time: 22:27
 */
namespace RedstoneTechnology\Utilities;

/**
 * Class RemoteFilesystem
 * @package RedstoneTechnology\Utilities
 */
class RemoteFilesystem
{
    /**
     * @var \Aws\S3\S3Client
     */
    protected $client;

    /**
     * RemoteFilesystem constructor.
     * @param \Aws\S3\S3Client $client
     */
    public function __construct(\Aws\S3\S3Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param $path
     * @param $container
     */
    public function push($path, $container)
    {
        $this->client->uploadDirectory($path, $container);
    }

    /**
     * @param $path
     * @param $container
     */
    public function pull($path, $container)
    {
        $this->client->downloadBucket($path, $container);
    }

    /**
     * @param $path
     * @param $container
     * @return \Aws\Result
     */
    public function get($path, $container)
    {
        return $this->client->getObject([
            'Bucket'    => $container,
            'Key'       => $path,
        ])['Body']->getContents();
    }

    /**
     * @param $path
     * @param $container
     * @return \Aws\Result
     */
    public function put($path, $container)
    {
        return $this->client->putObject([
            'Bucket'    => $container,
            'Key'       => $path,
        ]);
    }
}
