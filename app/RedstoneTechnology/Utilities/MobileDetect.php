<?php

namespace RedstoneTechnology\Utilities;

/**
 * This is a simple wrapper class, which uses the mobile detect class here:
 * https://github.com/serbanghita/Mobile-Detect
 * The idea of the wrapper is to allow us to change this implementation later, without changing it anywhere except
 * in this class.
 * Class MobileDetect
 * @package RedstoneTechnology\Utilities
 */
class MobileDetect
{
    protected $mobileDetect;

    /**
     * @param \Mobile_Detect $mobileDetect
     */
    public function __construct(
        \Mobile_Detect $mobileDetect
    ) {
        $this->mobileDetect = $mobileDetect;
    }

    /**
     * Returns true if the client is a mobile, false if not.
     * @return boolean
     */
    public function isMobile()
    {
        return $this->mobileDetect->isMobile();
    }
}
