<?php

namespace RedstoneTechnology\Utilities;

/**
 * Class Validate
 * @package RedstoneTechnology\Utilities
 */
class Validate
{
    /**
     * @param $type
     * @param $value
     * @return mixed
     */
    public function validateVal($type, $value)
    {
        $function = 'validate'.ucfirst($type);
        return($this->$function($value));
    }

    /**
     * @param $arg
     * @return mixed|string
     */
    public function validateStr($arg)
    {
        $arg = urldecode($arg);
        $arg = filter_var(
            $arg,
            FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_LOW
        );
        $arg = filter_var(
            $arg,
            FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_HIGH
        );
        /**
         * trim added to strip spaces at start or end, which causes an error in postcodes, confusing some users.
         */
        $arg = trim($arg);

        return $arg;
    }

    /**
     * @param $arg
     * @return int|mixed
     */
    public function validateInt($arg)
    {
        $arg = urldecode($arg);
        $return = filter_var(
            preg_replace("/[^0-9,.]/", "", $arg),
            FILTER_SANITIZE_NUMBER_FLOAT,
            FILTER_FLAG_ALLOW_FRACTION
        );
        if ($return === '') {
            $return = 0;
        }

        return $return;
    }

    /**
     * @param $arg
     * @return int|mixed
     */
    public function validatePosint($arg)
    {
        $arg = urldecode($arg);
        $return = filter_var(
            preg_replace("/[^0-9,.]/", "", $arg),
            FILTER_SANITIZE_NUMBER_FLOAT,
            FILTER_FLAG_ALLOW_FRACTION
        );
        if ($return === '') {
            $return = 1;
        }
        if ($return < 1) {
            $return = 1;
        }

        return $return;
    }

    /**
     * Takes a postal code, and a two digit country code, an will run the appropriate postal code validation.
     * @param $postalCode
     * @param $region
     * @return bool
     */
    public function validatePostalcode($postalCode, $region)
    {
        $function = "postalCode{$region}";
        $return = $this->$function($postalCode);
        return($return);
    }

    /**
     * @param $postalCode
     * @param $regions
     * @return array|bool
     */
    public function guessPostalRegion($postalCode, $regions)
    {
        foreach ($regions as $id => $region) {
            $function = "postalCode{$id}";
            if ($this->$function($postalCode)) {
                $return[] = $id;
            }
        }
        if (!isset($return)) {
            return false;
        }

        return $return;
    }

    /**
     * Takes a partial UK postalcode, and expands it to what should usually be a valid full postal code.
     * @param $item
     * @param $region
     * @return bool|mixed
     */
    public function expandPostalcode($item, $region)
    {
        if ($region === 'EBAY-GB') {
            $expandedPostalcode = preg_replace(
                '/^([A-PR-UWYZ]([0-9]([0-9]|[A-HJKSTUW])?|[A-HK-Y][0-9]))(.*)/i',
                '${1} 1AA',
                $item
            );

            return $expandedPostalcode;
        }

        return false;
    }

    /**
     * @param $postalCode
     * @return int
     */
    public function postalCodeAU($postalCode)
    {
        $match = preg_match('(^[0-9]{4}$)', $postalCode);
        return (in_array($match,[0, false]) ? false : true);
    }

    /**
     * @param $postalCode
     * @return int
     */
    public function postalCodeCA($postalCode)
    {
        $letters = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
        $sRegExp = "[ABCEGHJKLMNPRSTVXY][0-9][$letters]"
                 . "[ \t-]*[0-9][$letters][0-9]";

        $sRegExp = '/^' . $sRegExp . '$/';

        $match = preg_match($sRegExp, strtoupper($postalCode));
        return (in_array($match,[0, false]) ? false : true);
    }

    /**
     * @param $postalCode
     * @return int
     */
    public function postalCodeDE($postalCode)
    {
        // $strong is not used here at the moment; added for API compatibility
        // checks might be added at a later stage
        $match = preg_match('/^[0-9]{5}$/', $postalCode);
        return (in_array($match,[0, false]) ? false : true);
    }

    /*
     * This function validates postal district codes in Dublin.
     * It will be revised when national postal codes are rolled out.
     *
     */
    /**
     * @param $postalCode
     * @return int
     */
    public function postalCodeIE($postalCode)
    {
        $postalCode = strtoupper(str_replace(' ', '', trim($postalCode)));
        $postalCode = str_replace('DUBLIN', 'D', $postalCode);

        $match = preg_match('/^D[0-9]{1,2}$/', $postalCode);
        return (in_array($match,[0, false]) ? false : true);
    }

    /**
     * @param $postalCode
     * @return int
     */
    public function postalCodeGB($postalCode)
    {
        /**
         * remove spaces and uppercase it
         */
        $postalCode = strtoupper(str_replace(' ', '', $postalCode));
        $preg     = "/^([A-PR-UWYZ]([0-9]([0-9]|[A-HJKSTUW])?|[A-HK-Y][0-9]"
                  . "([0-9]|[ABEHMNPRVWXY])?)[0-9][ABD-HJLNP-UW-Z]{2}|GIR0AA)$/";
        $match    = preg_match($preg, $postalCode);
        return (in_array($match,[0, false]) ? false : true);
    }

    /**
     * @param $postalCode
     * @return int
     */
    public function postalCodeUS($postalCode)
    {
        $match = preg_match('/^[0-9]{5}((-| )[0-9]{4})?$/', $postalCode);
        return (in_array($match,[0, false]) ? false : true);
    }
}
